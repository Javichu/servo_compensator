#define DEBUG 0

// Cabeceras Arduino
#include <Wire.h>
#include <Servo.h>

//Direccion I2C de la IMU
#define MPU 0x68
 
//Ratios de conversion
#define A_R 16384.0
#define G_R 131.0
 
//Conversion de radianes a grados 180/PI
#define RAD_A_DEG = 57.295779
 
//MPU-6050 da los valores en enteros de 16 bits
//Valores sin refinar
int16_t AcX, AcY, AcZ, GyX, GyY, GyZ;
 
//Angulos
float Acc[2];
float Gy[2];
float Angle[2];

// Variables para control
const float WINDUP_GUARD_GAIN = 30.0;
float inclinacionActual = 0.0;
float inclinacionDeseada = 34.0;
const float offset = 32.6;
float error = 0.0;
float errorAnterior = 0.0;
float errorTotal = 0.0;
//float kP = 6.0;
//float kI = 1.1;
//float kD = 30.0;
float kP = 0.4;
float kI = 0.0;
float kD = 10.0;
int potenciaPWM = 0.0;
float windupGuard = 0.0;
char constanteCambio = 'p';
int posActual = 90;

// Variables Arduino
Servo myservo;
const int pinMotor1 = 9;

void setup()
{
  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  myservo.attach(pinMotor1);
  #ifdef DEBUG
    Serial.begin(9600);
  #endif
}

void loop()
{
   //Leer los valores del Acelerometro de la IMU
   Wire.beginTransmission(MPU);
   Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
   Wire.endTransmission(false);
   Wire.requestFrom(MPU,6,true); //A partir del 0x3B, se piden 6 registros
   AcX=Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
   AcY=Wire.read()<<8|Wire.read();
   AcZ=Wire.read()<<8|Wire.read();
 
    //Se calculan los angulos Y, X respectivamente.
   Acc[1] = atan(-1*(AcX/A_R)/sqrt(pow((AcY/A_R),2) + pow((AcZ/A_R),2)))*RAD_TO_DEG;
   Acc[0] = atan((AcY/A_R)/sqrt(pow((AcX/A_R),2) + pow((AcZ/A_R),2)))*RAD_TO_DEG;
 
   //Leer los valores del Giroscopio
   Wire.beginTransmission(MPU);
   Wire.write(0x43);
   Wire.endTransmission(false);
   Wire.requestFrom(MPU,4,true); //A diferencia del Acelerometro, solo se piden 4 registros
   GyX=Wire.read()<<8|Wire.read();
   GyY=Wire.read()<<8|Wire.read();
 
   //Calculo del angulo del Giroscopio
   Gy[0] = GyX/G_R;
   Gy[1] = GyY/G_R;
 
   //Aplicar el Filtro Complementario
   Angle[0] = 0.98 *(Angle[0]+Gy[0]*0.010) + 0.02*Acc[0];
   Angle[1] = 0.98 *(Angle[1]+Gy[1]*0.010) + 0.02*Acc[1];

   // Aplicar el offset
   inclinacionActual = offset - Angle[1];

   // Comprobamos si se cambia alguna constante por serie
   #ifdef DEBUG
     while (Serial.available() > 0) {
           constanteCambio = Serial.read();
           if (constanteCambio == 'p') {
            kP = Serial.parseFloat(); 
           }else if (constanteCambio == 'i') {
            kI = Serial.parseFloat(); 
           }else if (constanteCambio == 'd') {
            kD = Serial.parseFloat();
           }
     }
   #endif
   
   error = inclinacionDeseada - inclinacionActual;
   
   errorTotal = errorTotal + error;
   windupGuard = WINDUP_GUARD_GAIN;
   if(errorTotal > windupGuard){
      errorTotal = windupGuard;
    }else if(errorTotal < -windupGuard){
      errorTotal = -windupGuard;
      //errorTotal = 0.0;
    }
  
   potenciaPWM = kP * error + kI * errorTotal + kD * (error-errorAnterior);
   
   errorAnterior = error;
   
   if (potenciaPWM > 10){
     potenciaPWM = 10;
   }else if (potenciaPWM < -10){
     potenciaPWM = -10;
   }

   posActual = posActual - potenciaPWM;
   if (posActual > 170){
     posActual = 170;
   }else if (posActual < 10){
     posActual = 10;
   }
   myservo.write(posActual);

   #ifdef DEBUG
     //Mostrar los valores por consola
     //Serial.print("Angle X: "); Serial.print(Angle[0]); Serial.print("\n");
     //Serial.print("Angle Y: "); Serial.print(Angle[1]); Serial.print("\n");
     Serial.print("Incli: "); Serial.print(inclinacionActual); Serial.print("\n");
     Serial.print("Error: "); Serial.print(error); Serial.print("\n");
     Serial.print("Error anterior: "); Serial.print(errorAnterior); Serial.print("\n");
     Serial.print("Error total: "); Serial.print(errorTotal); Serial.print("\n");
     Serial.print("kP: "); Serial.print(kP); Serial.print("\n");
     Serial.print("kI: "); Serial.print(kI); Serial.print("\n");
     Serial.print("kD: "); Serial.print(kD); Serial.print("\n");
     Serial.print("PWM: "); Serial.print(potenciaPWM); Serial.print("\n");
     Serial.print("Pos: "); Serial.print(posActual);
     Serial.print("\n------------\n");
   #endif
   
   delay(10); //Nuestra dt sera, pues, 0.010, que es el intervalo de tiempo en cada medida
}

